# README #

Demo Multiversion Registration Api 2 accompanying source code for blog entry at http://tech.asimio.net/2017/03/06/Multi-version-Service-Discovery-using-Spring-Cloud-Netflix-Eureka-and-Ribbon.html

### Requirements ###

* Java 8
* Maven 3.3.x
* Eureka server as described at https://bitbucket.org/asimio/discoveryserver
* Demo Multiversion Registration Api 1 as described at https://bitbucket.org/asimio/demo-multiversion-registration-api-1

### Building and executing the application from command line ###

```
mvn clean package
java -DappPort=8701 -DhostName=$HOSTNAME -jar target/demo-multiversion-registration-api-2.jar
```

Eureka server listening on localhost:8000 otherwise would need to include VM arg similar to:
```
-Deureka.client.serviceUrl.defaultZone=http://localhost:8000/eureka/
```

### Who do I talk to? ###

* ootero at asimio dot net
* https://www.linkedin.com/in/ootero